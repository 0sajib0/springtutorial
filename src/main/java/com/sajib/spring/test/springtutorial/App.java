package com.sajib.spring.test.springtutorial;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/sajib/spring/test/springtutorial/bean/beans.xml");

		ContactBook contacts=(ContactBook) context.getBean("contactbook");
		
		System.out.println(contacts);

		((ClassPathXmlApplicationContext) context).close();
	}
}
