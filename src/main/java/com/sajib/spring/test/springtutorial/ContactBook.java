package com.sajib.spring.test.springtutorial;

import java.util.HashMap;
import java.util.Map;

public class ContactBook {
	private Map<String, EmergencyContacts> contacts= new HashMap<String, EmergencyContacts>();

	public Map<String, EmergencyContacts> getContacts() {
		return contacts;
	}

	public void setContacts(Map<String, EmergencyContacts> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		String string="";
		for(Map.Entry<String, EmergencyContacts>contact:contacts.entrySet())
		{
			string+=contact+"\n";
		}
		return string;
	}
	
	

}
