package com.sajib.spring.test.springtutorial;

import java.util.List;

public class Patient {
	private int id;
	private String name;
	
	private List<EmergencyContacts>contacts;
	
	private EmergencyContacts criticalcontracts;
	

	public Patient() {

	}
	
	
	public void init(){
		System.out.println("patient created "+this);
	}
	public void destroy(){
		System.out.println("Patient destroyed ");
	}

	public Patient(int id, String name) {
		// super();
		this.id = id;
		this.name = name;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void speak() {
		System.out.println("help! i am in danger");
	}


	public List<EmergencyContacts> getContacts() {
		return contacts;
	}


	public void setContacts(List<EmergencyContacts> contacts) {
		this.contacts = contacts;
	}


	public EmergencyContacts getCriticalcontracts() {
		return criticalcontracts;
	}


	public void setCriticalcontracts(EmergencyContacts criticalcontracts) {
		this.criticalcontracts = criticalcontracts;
	}


	@Override
	public String toString() {
		return "Patient [id=" + id + ", name=" + name + ", criticalcontracts=" + criticalcontracts + "]";
	}


	
	
	


	


}
